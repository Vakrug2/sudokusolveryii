<?php

use app\components\SudokuWidget;
use app\models\Sudoku;
use yii\helpers\Html;
use yii\web\View;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this View */
/* @var $model Sudoku */

$this->title = $model->datetime;
$this->params['breadcrumbs'][] = ['label' => 'Sudoku list', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);
?>
<div class="sudoku-view">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?= DetailView::widget([
       'model' => $model,
       'attributes' => [
           'notes:ntext',
       ],
   ]) ?>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::button('Solve', ['class' => 'btn btn-success', 'id' => 'sudoku-solve-btn']) ?>
    </p>
    
    <?= SudokuWidget::widget(['sudoku' => $model]) ?>

</div>
