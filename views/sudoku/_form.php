<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\SudokuWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Sudoku */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sudoku-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'data')->textarea(['rows' => 6])->input('hidden')->label(false)?>
    <?= $form->field($model, 'notes')->textarea(['rows' => 6]) ?>
    
    <?= SudokuWidget::widget(['sudoku' => $model, 'editable' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
