<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Sudoku */

$this->title = 'Create Sudoku';
$this->params['breadcrumbs'][] = ['label' => 'Sudoku list', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sudoku-create">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <ul>
        <li>Select cell with mouse click.</li>
        <li>Input number with keyboard</li>
        <li>Delete number with "0" or "Delete" key</li>
    </ul>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
