<?php
    use app\assets\SudokuAsset;
    SudokuAsset::register($this);
    
    /* @var $sudokuData array */
    /* @var $editable boolean */
?>

<table class="sudoku">
    <?php foreach($sudokuData as $keyRow => $row): ?>
    <tr>
        <?php foreach($row as $keyCol => $col): ?>
        <td row="<?= $keyRow ?>" col="<?= $keyCol ?>"><?= $col === 0 ? "" : $col ?></td>
        <?php endforeach; ?>
    </tr>
    <?php endforeach; ?>
</table>
<input type="hidden" id="sudoku-data" value="<?= json_encode($sudokuData) ?>" />
<?php
    if ($editable) {
        $script = <<< JS
            enableEditing();
JS;
        $this->registerJs($script);
    }
?>
<?php if (!$editable): ?>
<pre id="sudoku-solution"></pre>
    <?php
        $script = <<< JS
            enableSolveBtn('sudoku-solve-btn', 'sudoku-solution', 'sudoku-data');
JS;
        $this->registerJs($script);
    ?>
<?php endif; ?>
