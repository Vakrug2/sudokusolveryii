<?php

namespace app\components;

use yii\base\Widget;

class SudokuWidget extends Widget
{
    /** @var \app\models\Sudoku */
    public $sudoku;
    
    public $editable = false;

    public function init()
    {
        parent::init();
        if ($this->sudoku === null) {
            throw new \Exception("Sudoku model not provided");
        }
    }

    public function run()
    {
        $sudokuData = json_decode($this->sudoku->data);
        return $this->render('_sudoku', [
            'sudokuData' => $sudokuData,
            'editable' => $this->editable
        ]);
    }
}
