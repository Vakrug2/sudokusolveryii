<?php

namespace app\components;

use Exception;

class Sudoku {
    const STATE_CERTAIN = 0;
    const STATE_GUESS = 1;
    const STATE_SOLVED = 2;
    const STATE_WRONG = 3;
    
    public $field = [
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0]
    ];
    
    public $solution = "";
    public $foundNumbers = [];
    
    public $possibleNumbers = [
        [[],[],[],[],[],[],[],[],[]],
        [[],[],[],[],[],[],[],[],[]],
        [[],[],[],[],[],[],[],[],[]],
        [[],[],[],[],[],[],[],[],[]],
        [[],[],[],[],[],[],[],[],[]],
        [[],[],[],[],[],[],[],[],[]],
        [[],[],[],[],[],[],[],[],[]],
        [[],[],[],[],[],[],[],[],[]],
        [[],[],[],[],[],[],[],[],[]]
    ];
    
    public $state = self::STATE_CERTAIN;
    
    const POSSIBLE_NUMBERS_STATE_NONE = 0;
    const POSSIBLE_NUMBERS_STATE_FOUND_CERTAIN = 1;
    
    public $possibleNumbersState = self::POSSIBLE_NUMBERS_STATE_NONE;
    public $foundCertain = [ //[$info text $row 0..8, $col 0..8, $value 1..9]
        'info' => 'dummy',
        'row' => 10,
        'column' => 10,
        'value' => 10
    ]; 
    
    const GUESS_TYPE_MANY_NUMBERS_IN_ONE_CELL = 0;
    const GUESS_TYPE_ONE_NUMBER_IN_MANY_CELLS = 1;
    
    public $bestGuess = [ //[$guessType, $guessStrength 2..9 (less is better), $row 0..8, $col 0..8, $possibleNumbers [1..9, 1..9, 1..9, ...]]
        'type' => 999, //this will rise error
        'strength' => 10, //2..9 (less is better)
        //for type GUESS_TYPE_MANY_NUMBERS_IN_ONE_CELL
        'row' => 10, //0..8
        'column' => 10, //0..8
        'possibleNumbers' => [10, 11, 12], //[1..9, 1..9, 1..9, ...]
        //for type GUESS_TYPE_ONE_NUMBER_IN_MANY_CELLS
        'number' => 10, //1..9
        'possibleCells' => [ //[[10, 10], [11, 11], ...]
            [
                "row" => 10,
                "column" => 10,
            ],
            [
                "row" => 11,
                "column" => 11,
            ],
        ]
    ];
    
    public function __construct(array $field) {
        $this->field = $field;
    }
    
    /**     
     * @param type $number 1..9     
     */
    public function GetRow($number): array {
        return $this->field[$number - 1];
    }
    
    /**     
     * @param type $number 1..9
     */
    public function GetColumn($number): array {
        $ret = [];
        
        foreach ($this->field as $row) {
            $ret[] = $row[$number - 1];
        }
        
        return $ret;
    }
    
    /**     
     * @param int $number 1..9
     */
    public function GetColumnPossibleNumbers(int $number): array {
        $ret = [];
        
        foreach ($this->possibleNumbers as $row) {
            $ret[] = $row[$number - 1];
        }
        
        return $ret;
    }
    
    // number       number - 1
    // 1, 2, 3,     0, 1, 2,        row 0
    // 4, 5, 6,     3, 4, 5,        row 1
    // 7, 8, 9      6, 7, 8         row 2
    public function GetSquare(int $number): array {
        $num = $number - 1;
        $rowNum = intdiv($num, 3);
        $colNum = $num % 3;
        
        return $this->GetSquareByRowAndColNumbers($rowNum, $colNum);
    }
    
    /**     
     * @param int $number 1..9     
     */
    public function GetSquarePossibleNumbers(int $number): array {
        $num = $number - 1;
        $rowNum = intdiv($num, 3);
        $colNum = $num % 3;
        
        return $this->GetSquarePossibleNumbersByRowAndColNumbers($rowNum, $colNum);
    }
    
    /**     
     * @param int $row 0..8
     * @param int $col 0..8
     */
    public function GetSquareByValuePos(int $row, int $col): array {
        $rowNum = intdiv($row, 3);
        $colNum = intdiv($col, 3);
        
        return $this->GetSquareByRowAndColNumbers($rowNum, $colNum);
    }
    
    /**     
     * @param int $rowNum 0..2
     * @param int $colNum 0..2
     */
    public function GetSquareByRowAndColNumbers(int $rowNum, int $colNum): array {
        $ret = [];
        for ($i = 3 * $rowNum; $i <= 3 * $rowNum + 2; $i++) {
            for ($j = 3 * $colNum; $j <= 3 * $colNum + 2; $j++) {
                $ret[] = $this->field[$i][$j];
            }
        }
        return $ret;
    }
    
    /**     
     * @param int $rowNum 0..2
     * @param int $colNum 0..2
     */
    public function GetSquarePossibleNumbersByRowAndColNumbers(int $rowNum, int $colNum): array {
        $ret = [];
        for ($i = 3 * $rowNum; $i <= 3 * $rowNum + 2; $i++) {
            for ($j = 3 * $colNum; $j <= 3 * $colNum + 2; $j++) {
                $ret[] = $this->possibleNumbers[$i][$j];
            }
        }
        return $ret;
    }
    
    /**     
     * @param type $squareNumber 1..9
     * @param type $valuePos 0..8
     * @return array ['row' => $row (0..8), 'column' => $col (0..8)]
     */
    public function GetRowAndColNumberFromSquareBySquareNumberAndValuesPos($squareNumber, $valuePos): array {
        $row = intdiv($squareNumber - 1, 3) * 3;
        $row += intdiv($valuePos, 3);
        
        $col = ($squareNumber - 1) % 3 * 3;
        $col += $valuePos % 3;
        
        return ['row' => $row, 'column' => $col];
    }
    
    public function checkValuesCount($countValues): bool {
        foreach ($countValues as $value => $count) {
            if ($value === 0) {
                continue;
            }
            if ($count > 1) {
                return false;
            }
        }
        return true;
    }
    
    public function valid(): bool {
        //squares
        for ($i = 1; $i <= 9; $i++) {
            $square = $this->GetSquare($i);
            $countValues = array_count_values($square);
            if (!$this->checkValuesCount($countValues)) {
                return false;
            }
        }
        
        //rows
        for ($i = 1; $i <= 9; $i++) {
            $row = $this->GetRow($i);
            $countValues = array_count_values($row);
            if (!$this->checkValuesCount($countValues)) {
                return false;
            }
        }
        
        //columns
        for ($i = 1; $i <= 9; $i++) {
            $column = $this->GetColumn($i);
            $countValues = array_count_values($column);
            if (!$this->checkValuesCount($countValues)) {
                return false;
            }
        }
        
        return true;
    }
    
    public function calculatePossibleNumbers(): void {
        $solved = true;
        $this->possibleNumbersState = self::POSSIBLE_NUMBERS_STATE_NONE;
        $this->bestGuess = [
            'type' => 999,
            'strength' => 10,
            'row' => 10,
            'column' => 10,
            'possibleNumbers' => [10, 11, 12],
            'number' => 10,
            'possibleCells' => [
                [
                    "row" => 10,
                    "column" => 10,
                ],
                [
                    "row" => 11,
                    "column" => 11,
                ],
            ]
        ];
        for ($i = 0; $i <= 8; $i++) {
            for ($j = 0; $j <= 8; $j++) {
                if ($this->field[$i][$j] !== 0) {
                    continue;
                }
                $solved = false;
                $possibleNumbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];
                $square = $this->GetSquareByValuePos($i, $j);
                $row = $this->GetRow($i + 1);
                $col = $this->GetColumn($j + 1);
                
                $possibleNumbers = array_values(array_diff($possibleNumbers, $square, $row, $col));
                
                if (count($possibleNumbers) === 0) {
                    if ($this->state === self::STATE_CERTAIN) {
                        throw new Exception("TODO");
                    } else {                        
                        $this->state = self::STATE_WRONG;
                        return;
                    }
                }
                
                if (count($possibleNumbers)=== 1) {
                    $this->possibleNumbersState = self::POSSIBLE_NUMBERS_STATE_FOUND_CERTAIN;
                    $this->foundCertain = [
                        'info' => "Only possible number here",
                        'row' => $i,
                        'column' => $j,
                        'value' => $possibleNumbers[0]                        
                    ];
                    return;
                }
                
                $this->possibleNumbers[$i][$j] = $possibleNumbers;
                
                if ($this->bestGuess['strength'] > count($possibleNumbers)) {                    
                    $this->bestGuess = [
                        'type' => self::GUESS_TYPE_MANY_NUMBERS_IN_ONE_CELL,
                        'strength' => count($possibleNumbers),
                        'row' => $i,
                        'column' => $j,
                        'possibleNumbers' => $possibleNumbers
                    ];
                }
            }
        }
        
        if ($solved) {
            $this->state = self::STATE_SOLVED;
        }
    }
    
    public function analyzePossibleNumbers() {
        // for each number (1..9)
        for ($digit = 1; $digit <= 9; $digit++) {            
            //for each square, row and column
            //square
            for ($i = 1; $i <= 9; $i++) {
                $square = $this->GetSquare($i);
                if (in_array($digit, $square)) {
                    continue;
                }
                $possibleNumbersSquare = $this->GetSquarePossibleNumbers($i);
                $digitCanBeInPlacesInSquare = [];
                
                for ($j = 0; $j <= 8; $j++) {
                    if ($square[$j] !== 0) {
                        continue;
                    }
                    if (in_array($digit, $possibleNumbersSquare[$j])) {
                        $digitCanBeInPlacesInSquare[] = $j;
                    }
                }
                                
                if (count($digitCanBeInPlacesInSquare) === 0) {
                    if ($this->state === self::STATE_CERTAIN) {
                        throw new Exception("TODO");
                    } else {
                        $this->state = self::STATE_WRONG;
                        return;
                    }
                }
                
                if (count($digitCanBeInPlacesInSquare) === 1) {
                    $this->possibleNumbersState = self::POSSIBLE_NUMBERS_STATE_FOUND_CERTAIN;
                    $rowCol = $this->GetRowAndColNumberFromSquareBySquareNumberAndValuesPos($i, $digitCanBeInPlacesInSquare[0]);
                    $this->foundCertain = [
                        'info' => 'Only possible cell for that number in that square',
                        'row' => $rowCol['row'],
                        'column' => $rowCol['column'],
                        'value' => $digit                        
                    ];
                    return;
                }
                
                if (count($digitCanBeInPlacesInSquare) < $this->bestGuess['strength']) {
                    $possibleCells = [];
                    foreach($digitCanBeInPlacesInSquare as $place) {
                        $possibleCells[] = $place;
                    }
                    $rowCol = $this->GetRowAndColNumberFromSquareBySquareNumberAndValuesPos($i, $digitCanBeInPlacesInSquare[0]);                    
                    
                    $this->bestGuess = [
                        'type' => self::GUESS_TYPE_ONE_NUMBER_IN_MANY_CELLS,
                        'strength' => count($digitCanBeInPlacesInSquare),
                        'number' => $digit,
                        'possibleCells' => $possibleCells
                    ];
                }
            }
            
            //row
            for ($i = 1; $i <= 9; $i++) {
                $row = $this->GetRow($i);
                if (in_array($digit, $row)) {
                    continue;
                }
                $possibleNumbersRow = $this->possibleNumbers[$i-1];
                $digitCanBeInPlacesInRow = [];
                
                for ($j = 0; $j <= 8; $j++) {
                    if ($row[$j] !== 0) {
                        continue;
                    }
                    if (in_array($digit, $possibleNumbersRow[$j])) {
                        $digitCanBeInPlacesInRow[] = $j;
                    }
                }
                
                if (count($digitCanBeInPlacesInRow) === 0) {
                    if ($this->state === self::STATE_CERTAIN) {
                        throw new Exception("TODO");
                    } else {
                        $this->state = self::STATE_WRONG;
                        return;
                    }
                }
                
                if (count($digitCanBeInPlacesInRow) === 1) {
                    $this->possibleNumbersState = self::POSSIBLE_NUMBERS_STATE_FOUND_CERTAIN;                    
                    $this->foundCertain = [
                        'info' => 'Only possible cell for that number in that row',
                        'row' => $i-1,
                        'column' => $digitCanBeInPlacesInRow[0],
                        'value' => $digit                        
                    ];                    
                    return;
                }
                
                if (count($digitCanBeInPlacesInRow) < $this->bestGuess['strength']) {
                    $possibleCells = [];
                    foreach($digitCanBeInPlacesInRow as $column) {
                        $possibleCells[] = [
                            'row' => $i-1,
                            'column' => $column
                        ];
                    }
                    $this->bestGuess = [
                        'type' => self::GUESS_TYPE_ONE_NUMBER_IN_MANY_CELLS,
                        'strength' => count($digitCanBeInPlacesInRow),     
                        'number' => $digit,
                        'possibleCells' => $possibleCells
                    ];
                }
            }
            
            //column
            for ($i = 1; $i <= 9; $i++) {
                $column = $this->GetColumn($i);
                
                if (in_array($digit, $column)) {
                    continue;
                }
                $possibleNumbersColumn = $this->GetColumnPossibleNumbers($i);
                $digitCanBeInPlacesInColumn = [];
                
                for ($j = 0; $j <= 8; $j++) {
                    if ($column[$j] !== 0) {
                        continue;
                    }
                    if (in_array($digit, $possibleNumbersColumn[$j])) {
                        $digitCanBeInPlacesInColumn[] = $j;
                    }
                }
                
                if (count($digitCanBeInPlacesInColumn) === 0) {
                    if ($this->state === self::STATE_CERTAIN) {
                        throw new Exception("TODO");
                    } else {
                        $this->state = self::STATE_WRONG;
                        return;
                    }
                }
                
                if (count($digitCanBeInPlacesInColumn) === 1) {
                    $this->possibleNumbersState = self::POSSIBLE_NUMBERS_STATE_FOUND_CERTAIN;                    
                    $this->foundCertain = [
                        'info' => 'Only possible cell for that number in that column',
                        'row' => $digitCanBeInPlacesInColumn[0],
                        'column' => $i-1,
                        'value' => $digit                        
                    ];                       
                    return;
                }
                
                if (count($digitCanBeInPlacesInColumn) < $this->bestGuess['strength']) {                                        
                    $possibleCells = [];
                    foreach($digitCanBeInPlacesInColumn as $row) {
                        $possibleCells[] = [
                            'row' => $row,
                            'column' => $i - 1
                        ];
                    }
                    $this->bestGuess = [
                        'type' => self::GUESS_TYPE_ONE_NUMBER_IN_MANY_CELLS,
                        'strength' => count($digitCanBeInPlacesInColumn),
                        'number' => $digit,
                        'possibleCells' => $possibleCells
                    ];
                }
            }
        }
    }
    
    public function WriteCertain() {
        $this->field[$this->foundCertain['row']][$this->foundCertain['column']] = $this->foundCertain['value'];
    }
    /*
    public function printField() {
        echo "<pre>";
        for($i = 0; $i <= 8; $i++) {
            for($j = 0; $j <= 8; $j++) {                
                echo ($this->field[$i][$j] === 0 ? " " : $this->field[$i][$j]) . " ";
            }
            echo "<br />";
        }
        echo "</pre>";
    }
    */
    public function solve($debug = true, $levelDeep = 0) {        
        while ($this->state !== self::STATE_SOLVED) {            
            $this->calculatePossibleNumbers();
            if ($this->state === self::STATE_WRONG) {
                if ($debug) {
                    $this->solution .= str_repeat(" ", $levelDeep) . "WRONG!<br />";
                }
                return;
            }
            if ($this->state === self::STATE_SOLVED) {
                if ($debug) {
                    $this->solution .= str_repeat(" ", $levelDeep) . "SOLVED!<br />";
                }
                return;
            }
            if ($this->possibleNumbersState !== self::POSSIBLE_NUMBERS_STATE_FOUND_CERTAIN) {
                $this->analyzePossibleNumbers();
                if ($this->state === self::STATE_WRONG) {
                    if ($debug) {
                        $this->solution .= str_repeat(" ", $levelDeep) . "WRONG!<br />";
                    }
                    return;
                }
            }
            if ($this->state !== self::STATE_SOLVED && $this->possibleNumbersState === self::POSSIBLE_NUMBERS_STATE_FOUND_CERTAIN) {
                $this->WriteCertain();
                if ($debug) {
                    $this->solution .= str_repeat(" ", $levelDeep) . "Found {$this->foundCertain['value']} in [" . ($this->foundCertain['row'] + 1) . ", " . ($this->foundCertain['column'] + 1) . "] ({$this->foundCertain['info']})<br />";
                    $this->foundNumbers[] = [
                        'number' => $this->foundCertain['value'],
                        'row' => $this->foundCertain['row'],
                        'col' => $this->foundCertain['column']
                    ];
                }
                
            }            
            if ($this->state !== self::STATE_SOLVED && $this->possibleNumbersState === self::POSSIBLE_NUMBERS_STATE_NONE) {
                //then guessing time!                
                switch ($this->bestGuess['type']) {
                    case self::GUESS_TYPE_MANY_NUMBERS_IN_ONE_CELL:
                        $guessCount = count($this->bestGuess['possibleNumbers']);
                        for ($i = 0; $i <= $guessCount - 2; $i++) {
                            $sudoku = new self($this->field);
                            $sudoku->field[$this->bestGuess['row']][$this->bestGuess['column']] = $this->bestGuess['possibleNumbers'][$i];
                            $sudoku->foundNumbers[] = [
                                'number' => $this->bestGuess['possibleNumbers'][$i],
                                'row' => $this->bestGuess['row'],
                                'col' => $this->bestGuess['column']
                            ];
                            if ($debug) {
                                $this->solution .= str_repeat(" ", $levelDeep) . "Trying {$this->bestGuess['possibleNumbers'][$i]} in [" . ($this->bestGuess['row'] + 1) . ", " . ($this->bestGuess['column'] + 1) . "]";
                                $this->solution .= " Other numbers here can be: ";
                                foreach(array_slice($this->bestGuess['possibleNumbers'], $i + 1) as $posNum) {
                                    $this->solution .= $posNum . " ";
                                }
                                $this->solution .= "<br />";
                            }
                            $sudoku->state = self::STATE_GUESS;
                            $sudoku->solve($debug, $levelDeep + 1);
                            if ($sudoku->state === self::STATE_SOLVED) {
                                $this->field = $sudoku->field;
                                $this->state = self::STATE_SOLVED;
                                $this->solution .= $sudoku->solution;
                                $this->foundNumbers = array_merge($sudoku->foundNumbers, $this->foundNumbers);
                                return;
                            }
                            if ($sudoku->state === self::STATE_WRONG) {
                                $this->solution .= $sudoku->solution;
                                continue;
                            }
                            
                            throw new Exception("You are not supposed to be here!");
                        }
                        $this->foundCertain = [
                            'info' => 'Other numbers here failed',
                            'row' => $this->bestGuess['row'],
                            'column' => $this->bestGuess['column'],
                            'value' => $this->bestGuess['possibleNumbers'][$guessCount - 1]
                        ];
                        $this->possibleNumbersState = self::POSSIBLE_NUMBERS_STATE_FOUND_CERTAIN;
                        $this->WriteCertain();
                        if ($debug) {
                            $this->solution .= str_repeat(" ", $levelDeep) . "Found {$this->foundCertain['value']} in [" . ($this->foundCertain['row'] + 1) . ", " . ($this->foundCertain['column'] + 1) . "] ({$this->foundCertain['info']})<br />";
                            $this->foundNumbers[] = [
                                'number' => $this->foundCertain['value'],
                                'row' => $this->foundCertain['row'],
                                'col' => $this->foundCertain['column']
                            ];
                        }
                        break;
                    case self::GUESS_TYPE_ONE_NUMBER_IN_MANY_CELLS:
                        $guessCount = count($this->bestGuess['possibleCells']);
                        for ($i = 0; $i <= $guessCount - 2; $i++) {
                            $sudoku = new self($this->field);
                            $sudoku->field[$this->bestGuess['possibleCells'][$i]['row']][$this->bestGuess['possibleCells'][$i]['column']] = $this->bestGuess['number'];
                            $sudoku->foundNumbers[] = [
                                'number' => $this->bestGuess['number'],
                                'row' => $this->bestGuess['possibleCells'][$i]['row'],
                                'col' => $this->bestGuess['possibleCells'][$i]['column']
                            ];
                            if ($debug) {
                                $this->solution .= str_repeat(" ", $levelDeep) . "Trying {$this->bestGuess['number']} in [" . ($this->bestGuess['possibleCells'][$i]['row'] + 1) . ", " . ($this->bestGuess['possibleCells'][$i]['column'] + 1) . "]<br />";
                                $this->solution .= " This number can alse go to: ";
                                foreach(array_slice($this->bestGuess['possibleCells'], $i + 1) as $posCell) {
                                    $this->solution .= "[{$posCell['row']}, {{$posCell['column']}]";
                                }
                            }
                            $sudoku->state = self::STATE_GUESS;
                            $sudoku->solve($debug, $levelDeep + 1);
                            if ($sudoku->state === self::STATE_SOLVED) {
                                $this->field = $sudoku->field;
                                $this->state = self::STATE_SOLVED;
                                $this->solution .= $sudoku->solution;
                                $this->foundNumbers = array_merge($sudoku->foundNumbers, $this->foundNumbers);
                                return;
                            }
                            if ($sudoku->state === self::STATE_WRONG) {
                                $this->solution .= $sudoku->solution;
                                continue;
                            }
                            
                            throw new Exception("You are not supposed to be here!");
                        }
                        $this->foundCertain = [
                            'info' => 'other cells failed for that number',
                            'row' => $this->bestGuess['possibleCells'][$guessCount - 1]['row'],
                            'column' => $this->bestGuess['possibleCells'][$guessCount - 1]['column'],
                            'value' => $this->bestGuess['number']
                        ];
                        $this->possibleNumbersState = self::POSSIBLE_NUMBERS_STATE_FOUND_CERTAIN;
                        $this->WriteCertain();
                        if ($debug) {
                            $this->solution .= str_repeat(" ", $levelDeep) . "Found {$this->foundCertain['value']} in [" . ($this->foundCertain['row'] + 1) . ", " . ($this->foundCertain['column'] + 1) . "] " . "({$this->foundCertain['info']})<br />";
                            $this->foundNumbers[] = [
                                'number' => $this->foundCertain['value'],
                                'row' => $this->foundCertain['row'],
                                'col' => $this->foundCertain['column']
                            ];
                        }
                        break;
                    default:
                        throw new Exception('TODO');
                }
            }
        }
    }
}
 