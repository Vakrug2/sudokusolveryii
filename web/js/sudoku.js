var $activeCell = null;
var $sudokuData = null;
var $solutionP = null;

function enableEditing() {
    $("table.sudoku td").click(cellOnClick);
    
    $("body").keydown(onKeyDown);
    
    $sudokuData = $("#sudoku-data");
}

function cellOnClick(event) {
    makeActiveCell($(event.target));
}

function makeActiveCell(activeCell) {
    $activeCell = activeCell;
    $("table.sudoku td").removeClass("selected");
    $activeCell.addClass("selected");
}

function moveCursor(direction) {
    if ($activeCell === null) {
        return;
    }
    var posRow = parseInt($activeCell.attr("row"));
    var posCol = parseInt($activeCell.attr("col"));
    
    switch (direction) {
        case "ArrowUp":
            if (posRow > 0) {
                posRow--;
            }
            break;
        case "ArrowDown":
            if (posRow < 8) {
                posRow++;
            }
            break;
        case "ArrowLeft":
            if (posCol > 0) {
                posCol--;
            }
            break;
        case "ArrowRight":
            if (posCol < 8) {
                posCol++;
            }
            break;
    }
    makeActiveCell($("table.sudoku td[row='" + posRow + "'][col='" + posCol + "']"));
    
}

function onKeyDown(event) {
    if ($activeCell !== null) {
        var number = parseInt(event.key);        
        if (number >= 1 && number <= 9) {
            $activeCell.text(number);
            updateSudokuData($activeCell.attr('row'), $activeCell.attr('col'), number);            
        }
        if (number === 0 || event.key === "Delete" || event.key === "Backspace" || event.key === " ") {
            $activeCell.text("");
            updateSudokuData($activeCell.attr('row'), $activeCell.attr('col'), 0);
        }
        if (event.key === "ArrowLeft" || event.key === "ArrowUp" || event.key === "ArrowRight" || event.key === "ArrowDown") {
            moveCursor(event.key);
        }
    }
}

function updateSudokuData(row, col, value) {
    var data = JSON.parse($sudokuData.val());
    data[row][col] = value;
    $sudokuData.val(JSON.stringify(data));
}

function enableSolveBtn(btnId, solutionId, sudokuDataId) {
    $('#'+btnId).click(solve);
    $solutionP = $('#'+solutionId);
    $sudokuData = $('#'+sudokuDataId);
}

function solve() {
    $.post('/sudoku/solve', {
        'sudokuData': $sudokuData.val()
    }, function(data) {        
        $solutionP.html(data.solution);
        if (data.error) {            
            alert("Failed to solve!");
        }
        
        $sudokuTable = $("table.sudoku");
        data.foundNumbers.forEach(function(element) {
            $cell = $sudokuTable.find("td[row=" + element['row'] + "][col=" + element['col'] + "]");
            $cell.html('<span style="color: blue; font-size: 2em; font-style: italic;">' + element['number'] + '</span>');
        });
    });
}