<?php

namespace app\controllers;

use Yii;
use app\models\Sudoku;
use app\models\SudokuSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\Sudoku as SudokuObj;

/**
 * SudokuController implements the CRUD actions for Sudoku model.
 */
class SudokuController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sudoku models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SudokuSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Sudoku model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Sudoku model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Sudoku();
        $model->data = Sudoku::DefaultData();
        $model->datetime = \date('Y-m-d H:i:s');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Sudoku model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->datetime = \date('Y-m-d H:i:s');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Sudoku model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionSolve() {
        $request = Yii::$app->request;
        $sudokuData = $request->post('sudokuData'); 
        
        $Sudoku = new SudokuObj(json_decode($sudokuData));
        $solution = [];
        try {
            $Sudoku->solve();
            $solution['error'] = false;
            $solution['field'] = $Sudoku->field;
            $solution['solution'] = $Sudoku->solution;
            $solution['foundNumbers'] = $Sudoku->foundNumbers;
        } catch (\Exception $ex) {
            $solution['error'] = true;
            $solution['field'] = $Sudoku->field;
            $solution['solution'] = $Sudoku->solution;
            $solution['foundNumbers'] = $Sudoku->foundNumbers;
        }
        
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
        $response->data = $solution;
        
        return $response;        
    }

    /**
     * Finds the Sudoku model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sudoku the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sudoku::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
