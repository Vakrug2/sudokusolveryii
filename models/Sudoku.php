<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sudoku".
 *
 * @property int $id
 * @property string $data
 * @property string $datetime
 */
class Sudoku extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sudoku';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['data', 'datetime'], 'required'],
            [['data', 'datetime', 'notes'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'data' => 'Data',
            'datetime' => 'Datetime',
        ];
    }

    /**
     * {@inheritdoc}
     * @return SudokuQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SudokuQuery(get_called_class());
    }
    
    public static function DefaultData() {
        $emptyField = [];
        for ($row = 0; $row <= 8; $row++) {            
            for ($col = 0; $col <= 8; $col++) {                
                $emptyField[$row][$col] = 0;
            }
        }
        return json_encode($emptyField);
    }
}
